import sbt._

object Dependencies {

  lazy val smile = {
    val version = "1.4.0"
    Seq(
      "com.github.haifengl" %% "smile-scala"  % version,
      "com.github.haifengl" %  "smile-netlib" % version
    )
  }

  lazy val enumeratum = {
    val enumeratumVersion = "1.5.12"
    Seq("com.beachape" %% "enumeratum" % enumeratumVersion)
  }

  lazy val pureCSV = Seq("com.github.melrief" %% "purecsv" % "0.1.0")

  lazy val logback = Seq("ch.qos.logback" % "logback-classic" % "1.2.3")

  lazy val shapeless = Seq("com.chuusai" %% "shapeless" % "2.3.2")
}
