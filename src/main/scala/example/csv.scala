package example

import purecsv.unsafe._
import purecsv.unsafe.converter._
import StringConverterUtils.mkStringConverter

object CsvConverters {

  implicit def rowNameConverter[O](implicit tconv: StringConverter[O]) = new StringConverter[RowName[O]] {
    def from(i: String): RowName[O] = RowName(tconv.from(i))
    def to(rn: RowName[O]): String = tconv.to(rn.value)
  }

  implicit val survivedConverter = mkStringConverter[Survived](s => Survived.withValue(s.toInt), _.value.toString)

  implicit val genderConverter = mkStringConverter[Sex](Sex.withName, _.toString.toLowerCase)

  implicit val passengerClassConverter = mkStringConverter[PassengerClass](s => PassengerClass.withValue(s.toInt), _.value.toString)
}

