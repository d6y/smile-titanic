package example

import smile.classification.DecisionTree
import smile.data.{Attribute, NumericAttribute, NominalAttribute, AttributeDataset}
import smile.data.parser.ArffParser
import java.io.File
import java.nio.file.{Paths, Files}

object UnsafeExample {

  def titanic(trainFile: File, testFile: File, outFile: File): Unit = {

      // The 2nd column (index 1) in the data is the survived/died flag we want to predict for:
      val responseIndex = 1

      val parser = new ArffParser()
      parser.setResponseIndex(responseIndex)
      val passengers: AttributeDataset = parser.parse(trainFile)

      // unZipInt is in the smile.data package as an enrichment of Dataset
      val (xs, ys) = passengers.unzipInt

      val maxLeafNodes = 32 
      val tree = new DecisionTree(passengers.attributes(), xs, ys, maxLeafNodes)

      val correctCount = (xs zip ys).filter { case (passenger, outcome) => tree.predict(passenger) == outcome }.length

      println(
        s"""
        |Total: ${xs.length}
        |Correct: ${correctCount}
        |Percent: ${100.0 * (correctCount.toDouble / xs.length.toDouble)}
        """.stripMargin
      )

      // Write GraphViz representation
      val dotFilename = s"tree-${maxLeafNodes}.dot"
      val pngFilename = s"tree-${maxLeafNodes}.png"
      val dot: Array[Byte] = tree.dot().getBytes
      Files.write(Paths.get(dotFilename), dot)
      import sys.process._
      println(s"dot -Tpng $dotFilename > $pngFilename")
      

      // Write to Kaggle output format:
      val testData: AttributeDataset = (new ArffParser()).parse(testFile)
      val test = testData.unzip

      import purecsv.unsafe._
      case class Prediction(id: Int, survived: Int)
      val predictions = test.map{ example => Prediction(example(0).toInt, tree.predict(example)) }.toSeq
      predictions.writeCSVToFile(outFile, header=Some(List("PassengerId", "Survived")))
  }

  def main(args: Array[String]): Unit = {
   titanic(
      new File("src/main/resources/arff/train.arff"),
      new File("src/main/resources/arff/test.arff"),
      new File("predict.csv")
    )
  }

}
