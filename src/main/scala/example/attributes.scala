package example

import smile.data.{Attribute, NumericAttribute, NominalAttribute, AttributeDataset}
import enumeratum.EnumEntry
import enumeratum.values.{ValueEnum, ValueEnumEntry, IntEnum, IntEnumEntry}
import shapeless._
import shapeless.labelled.FieldType

object EnumAux {

  // FROM: https://github.com/milessabin/shapeless/blob/master/examples/src/main/scala/shapeless/examples/enum.scala
  trait Aux[T, Repr] {
    def values: List[T]
  }

  object Aux {
    implicit def cnilAux[A]: Aux[A, CNil] =
      new Aux[A, CNil] { def values = Nil }

    implicit def cconsAux[T, L <: T, R <: Coproduct]
      (implicit l: Witness.Aux[L], r: Aux[T, R]): Aux[T, L :+: R] =
      new Aux[T, L :+: R] { def values = l.value :: r.values }
  }
  // END FROM
}

object AttributeMakers {

  import EnumAux._
  
  trait AttributeMaker[T] {
    def attributes: List[Attribute]
  }

  implicit val nilMaker = new AttributeMaker[HNil] { def attributes = Nil }

  implicit def consMaker[K <: Symbol, H, T <: HList](implicit hm: AttributeMaker[FieldType[K,H]], tm: AttributeMaker[T]) =
    new AttributeMaker[FieldType[K,H] :: T] {
      def attributes: List[Attribute] = hm.attributes ::: tm.attributes
    }

  implicit def numericMaker[K <: Symbol, N : Numeric](implicit field: Witness.Aux[K]) = new AttributeMaker[FieldType[K,N]] {
    def attributes = new NumericAttribute(field.value.name) :: Nil
  }

  implicit def enumMaker[K <: Symbol, E <: EnumEntry, C](implicit field: Witness.Aux[K], cogen: Generic.Aux[E,C], enum: Aux[E,C]) =
    new AttributeMaker[FieldType[K, E]] {
      def attributes =
        new NominalAttribute(field.value.name, enum.values.map(_.entryName).toArray) :: Nil
    }

  implicit def intEnumMaker
    [K <: Symbol, E <: IntEnumEntry, C]
    (implicit field: Witness.Aux[K], cogen: Generic.Aux[E,C], enum: Aux[E,C]) =
      new AttributeMaker[FieldType[K, E]] {
        def attributes =
          new NominalAttribute(field.value.name, enum.values.map(_.value.toString).toArray) :: Nil
      }

  implicit def optMaker[K <: Symbol, O](implicit om: AttributeMaker[FieldType[K, O]]) = new AttributeMaker[FieldType[K, Option[O]]] {
    def attributes = om.attributes
  }

  // Strings and RowNames are not included in the model
  implicit def strMaker[K <: Symbol] = new AttributeMaker[FieldType[K, String]] { def attributes = Nil }
  implicit def rowNameMaker[K <: Symbol, V] = new AttributeMaker[FieldType[K, RowName[V]]] { def attributes = Nil }


  implicit def deriveAttributeMaker[P,H <: HList](implicit gen: LabelledGeneric.Aux[P,H], attr: AttributeMaker[H]) =
    new AttributeMaker[P] {
      def attributes = attr.attributes
    }

  def attributes[P](implicit converter: AttributeMaker[P]): List[Attribute] = converter.attributes
}
