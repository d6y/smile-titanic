package example

import smile.classification.DecisionTree
import smile.data.{Attribute, NumericAttribute, NominalAttribute, AttributeDataset}
import smile.data.parser.DelimitedTextParser
import java.io.File

import purecsv.unsafe._
import purecsv.unsafe.converter._

object TreeExample {

  def titanic(trainFile: File, testFile: File, outFile: File): Unit = {

    import AttributeMakers._
    val attrs: List[Attribute] = attributes[Passenger]  
    println(attrs)

    import CsvConverters._
    val records: Seq[Passenger] = CSVReader[Passenger].readCSVFromFile(trainFile, skipHeader=true)


    val name = "whatever"
    val response = attrs(1)
    val data: AttributeDataset = new AttributeDataset(name, attrs.toArray, response)

    //type Row = smile.data.Datum[Array[Double]]
    type Row = List[Double]

    import shapeless._
    import EnumAux._
    import enumeratum.EnumEntry
    import enumeratum.values.{ValueEnum, ValueEnumEntry, IntEnum, IntEnumEntry}

    trait RowConverter[P, T] {
      def convert(row: P): Row
    }

    def mkConverter[A,S](func: A => Row) = new RowConverter[A,S] {
      def convert(a: A) = func(a)
    }

    implicit def nilConverter[S] = mkConverter[HNil, S] { _ =>  Nil }

    implicit def consConverter[H, T <: HList, S](implicit hc: RowConverter[H,S], tc: RowConverter[T,S]) =
      mkConverter[H :: T, S] { case h :: t => hc.convert(h) ::: tc.convert(t) }

    implicit def numericConverter[N, S](implicit numeric: Numeric[N]) = mkConverter[N,S] {
      n => List(numeric.toDouble(n))
    }

    implicit def enumConverter[E <: EnumEntry, C, S](implicit cogen: Generic.Aux[E,C], enum: Aux[E,C]) =
      mkConverter[E,S] {
        e => List(1d)
      }

    implicit def deriveConverter[P, S, H <: HList](implicit gen: Generic.Aux[P,H], conv: RowConverter[H,S]) =
      new RowConverter[P,S] {
        def convert(row: P) = conv.convert(gen.to(row))
      }

  // and so on

    def rowConverter[P, S](implicit conv: RowConverter[P,S]): P => Row = p => conv.convert(p) 

    case class Test(x: Int, y: Sex)
    val f = rowConverter[Test, Survived]
    // println(x)


    /*
      // The 1st column (index 0) is a record ID and ignored
      // The 2nd column (index 1) in the data is the survived/died flag we want to predict for:

      val survival = new NominalAttribute("survived")

      val passengers: AttributeDataset =
        parser.parse(trainFile)
        //parser.setResponseIndex(survival,1).parse(trainFile)

      println(passengers.attributes().toList)

      // unZipInt is in the smile.data package as an enrichment of Dataset
      val (xs, ys) = passengers.unzipInt

      val tree = new DecisionTree(passengers.attributes(), xs, ys, 4)

      val correctCount = (xs zip ys).filter { case (passenger, outcome) => tree.predict(passenger) == outcome }.length

      println(
        s"""
        |Total: ${xs.length}
        |Correct: ${correctCount}
        |Percent: ${100.0 * (correctCount.toDouble / xs.length.toDouble)}
        |
        |${tree.dot()}
        """.stripMargin
      )

      // Write to Kaggle output format:
      val testData: AttributeDataset = parser.parse(testFile)
      val test = testData.unzip

      case class Prediction(id: Int, survived: Int)
      test.map{ example => Prediction(example(0).toInt, tree.predict(example)) }.toSeq.writeCSVToFile(outFile, header=Some(List("PassengerId", "Survived")))
      */
  }

  def main(args: Array[String]): Unit = {
   titanic(
      new File("src/main/resources/kaggle/train.csv"),
      new File("src/main/resources/test.arff"),
      new File("predict.csv")
    )
  }

}
