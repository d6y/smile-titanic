package example

import smile.classification.DecisionTree
import smile.data.{Attribute, NumericAttribute, NominalAttribute, AttributeDataset}
import smile.data.parser.DelimitedTextParser
import java.io.File
import java.nio.file.{Paths, Files}

object UnsafeCleanedExample {

  def titanic(trainFile: File, testFile: File, outFile: File): Unit = {

    val responseIndex = 8

    val attributes = Seq(
      new NominalAttribute("class"),
      new NominalAttribute("gender"),
      new NumericAttribute("age"),
      new NumericAttribute("sibsp"),
      new NumericAttribute("parch"),
      new NumericAttribute("fare"),
      new NominalAttribute("embarked")
    ).toArray

    def mkParser = 
      new DelimitedTextParser()
        .setDelimiter(",")
        .setRowNames(true)
        .setColumnNames(true)

    val parser = mkParser.setResponseIndex(new NominalAttribute("survival"), responseIndex)
    val passengers: AttributeDataset = parser.parse(attributes, trainFile)

    val (xs, ys) = passengers.unzipInt

    val maxLeafNodes = 6 
    val tree = new DecisionTree(passengers.attributes(), xs, ys, maxLeafNodes)

    val correctCount = (xs zip ys).filter { case (passenger, outcome) => tree.predict(passenger) == outcome }.length

    println(
      s"""
      |Total: ${xs.length}
      |Correct: ${correctCount}
      |Percent: ${100.0 * (correctCount.toDouble / xs.length.toDouble)}
      """.stripMargin
    )

    val dotFilename = s"tree-cleaned-${maxLeafNodes}.dot"
    val pngFilename = s"tree-cleaned-${maxLeafNodes}.png"
    val dot: Array[Byte] = tree.dot().getBytes
    Files.write(Paths.get(dotFilename), dot)
    import sys.process._
    println(s"dot -Tpng $dotFilename > $pngFilename")
    

    val testData: AttributeDataset = mkParser.parse(attributes, testFile)
    val test = testData.unzip

    import purecsv.unsafe._
    case class Prediction(id: Int, survived: Int)
    val predictions = test.map{ example => Prediction(example(0).toInt, tree.predict(example)) }.toSeq
    predictions.writeCSVToFile(outFile, header=Some(List("PassengerId", "Survived")))
  }

def main(args: Array[String]): Unit = {
 titanic(
    new File("src/main/resources/kaggle/cleaned/train.csv"),
    new File("src/main/resources/kaggle/cleaned/test.csv"),
    new File("predict.csv")
  )
}

}
