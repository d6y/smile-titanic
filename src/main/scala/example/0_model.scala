package example

/*
 * This file as a weird filename as a work around until Scala 2.12.4 ships
 * (with https://github.com/scala/scala/pull/6029).
 *
 * The 0_ part means it is compiled before `main.scala`, which
 * avioids a compile error of:
 * knownDirectSubclasses of Survived observed before subclass No registered
 */

import enumeratum._
import enumeratum.values.{ValueEnum, ValueEnumEntry, IntEnum, IntEnumEntry}
import enumeratum.EnumEntry.Lowercase

final case class RowName[T](value: T)

sealed abstract class Survived(val value: Int) extends IntEnumEntry
object Survived extends IntEnum[Survived] {
  val values = findValues
  case object No  extends Survived(0)
  case object Yes extends Survived(1)
}

sealed trait Sex extends EnumEntry with Lowercase
object Sex extends Enum[Sex] {
  val values = findValues
  case object Male   extends Sex
  case object Female extends Sex
}

sealed abstract class PassengerClass(val value: Int) extends IntEnumEntry
object PassengerClass extends IntEnum[PassengerClass] {
  val values = findValues
  case object First  extends PassengerClass(1)
  case object Second extends PassengerClass(2)
  case object Third  extends PassengerClass(3)
}

case class Passenger(
  passengerId : RowName[Int],   // Row identifier, from 1 to 1309
  survived    : Survived,       // 0 for no, 1 for yes
  pClass      : PassengerClass, // Ticket class: 1,2 or 3 (1st, 2nd, 3rd)
  name        : String,
  sex         : Sex,
  age         : Option[Float],  // Years: fraction if less than 1 or estimated
  sibSip      : Int,            // Siblings per spouce
  parCh       : Int,            // Parent per child
  ticket      : String,         // Ticket number such as "PC 17599"
  fare        : Option[Float],  // Complicated. See https://www.kaggle.com/c/titanic/discussion/33087
  cabin       : Option[String], // Cabin number, such as "C22"
  embarked    : Option[String], // Port, C = Cherbourg, Q = Queenstown, S = Southampton
)

