import Dependencies._

lazy val root = (project in file(".")).
  settings(
    inThisBuild(List(
      organization := "com.dallaway",
      scalaVersion := "2.12.3",
      version      := "1.0.0"
    )),
    name := "titanic",
    libraryDependencies ++= smile ++ enumeratum ++ pureCSV ++ shapeless ++ logback
  )
