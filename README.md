# Decision Trees with SMILE

Three variations:

1. unsafe.scala - reads ARFF data, unfiltered, and will produce a tree that includes the passenger id.

2. unsafecleaned.scala - reads cleaned up data, without cabins/names, and with the passenger ID as a row id.

3. main.scala - start of deriving attributes for a case class. Next steps would be to
  derive a row => AttributeDataset.Rom function that handles cases like the row name.




